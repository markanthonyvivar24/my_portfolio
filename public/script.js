"use strict";
$(document).ready(function () {
  // Animation for navbar toggler
  $(".nav-button").click(function (event) {
    event.preventDefault();
    $(this).toggleClass("change");
  });

  // To give animation to navbar when you scroll it
  $(window).scroll(function (event) {
    event.preventDefault();
    let position = $(this).scrollTop();

    if (position >= 200) {
      $(".navbar").addClass("custom-navbar");
    } else {
      $(".navbar").removeClass("custom-navbar");
    }
  });

  // To give animation to my skills section
  $(window).scroll(function (event) {
    event.preventDefault();
    let position = $(this).scrollTop();

    if (position >= 100) {
      $(".skill1").addClass("fromTop");
      $(".skill2").addClass("fromDown");
      $(".skill3").addClass("fromTop");
    } else {
      $(".skill1").removeClass("fromTop");
      $(".skill2").removeClass("fromDown");
      $(".skill3").removeClass("fromTop");
    }
  });

  // To see the specific images according to category
  $(".technology-list-item").click(function (event) {
    event.preventDefault();
    let value = $(this).attr("data-filter");

    if (value === "all") {
      $(".filter").show(300);
    } else {
      $(".filter")
        .not("." + value)
        .hide(300);
      $(".filter")
        .filter("." + value)
        .show(300);
    }
  });

  // To see the color of active item to your category list
  $(".technology-list-item").click(function (event) {
    event.preventDefault();
    $(this).addClass("active-item").siblings().removeClass("active-item");
  });
});
